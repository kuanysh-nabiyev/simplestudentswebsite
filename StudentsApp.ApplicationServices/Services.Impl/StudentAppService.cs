﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using StudentsApp.Persistance;
using System.Data.Entity;

namespace StudentsApp.ApplicationServices.Services.Impl
{
    public class StudentAppService : IStudentAppService
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ITeacherRepository _teacherRepository;
        private readonly IUserRepository _userRepository;

        public StudentAppService(IStudentRepository studentRepository, 
            ITeacherRepository teacherRepository, 
            IUserRepository userRepository)
        {
            _studentRepository = studentRepository;
            _teacherRepository = teacherRepository;
            _userRepository = userRepository;
        }

        public IEnumerable<StudentListViewModel> GetAllStudents()
        {
            using (var db = new StudentsAppDatabase())
            {
                var students =
                    from student in db.Students
                    select new StudentListViewModel()
                    {
                        StudentUserId = student.StudentUserId,
                        StudentFullName = student.FirstName + " " + student.SurName,
                        AverageMark = student.Marks.Average(a => a.Mark) ?? 0
                    };

                return students.ToList();
            }
        }

        public IEnumerable<StudentListViewModel> GetStudentsWhoHaveGreaterThanAverageMark()
        {
            var allStudents = GetAllStudents().ToList();
            double allStudentsAverageMark = allStudents.Average(st => st.AverageMark);
            var result = allStudents.Where(st => st.AverageMark > allStudentsAverageMark);

            return result;
        }

        public StudentDetailsViewModel GetStudentDetails(int studentUserId)
        {
            Student student = _studentRepository.GetDetailsById(studentUserId);
            if (student == null)
            {
                throw new Exception(String.Format("Студент {0} не найден в БД", studentUserId));
            }
            student.User = _userRepository.GetById(studentUserId);

            IEnumerable<TeacherViewModel> studentTeachers = student.Marks.Select(mark =>
                new TeacherViewModel()
                {
                    FirstName = mark.Teacher.FirstName,
                    SurName = mark.Teacher.SurName,
                    TeacherUserId = mark.Teacher.TeacherUserId
                });

            return new StudentDetailsViewModel()
            {
                StudentUserId = student.StudentUserId,
                Login = student.User.Login,
                FirstName = student.FirstName,
                SurName = student.SurName,
                Teachers = studentTeachers.ToList()
            };
        }

        public SaveResultViewModel SaveStudent(StudentDetailsViewModel studentDetailsViewModel)
        {
            using (var db = new StudentsAppDatabase())
            {
                var student = _studentRepository.GetDetailsById(studentDetailsViewModel.StudentUserId);
                db.Entry(student).State = EntityState.Modified;

                student.UpdateProfile(studentDetailsViewModel.FirstName, studentDetailsViewModel.SurName);

                foreach (TeacherViewModel teacherViewModel in studentDetailsViewModel.Teachers)
                {
                    bool isNewTeacherToStudent =
                        student.Marks.FirstOrDefault(a => a.Teacher.TeacherUserId == teacherViewModel.TeacherUserId) ==
                        null;

                    if (isNewTeacherToStudent)
                    {
                        var teacher = _teacherRepository.GetById(teacherViewModel.TeacherUserId);
                        db.Entry(teacher).State = EntityState.Unchanged;
                        db.StudentMarks.Add(StudentMark.NewStudentAttendsTeacher(student, teacher));
                    }
                }

                if (db.SaveChanges() > 0)
                {
                    Task.Delay(2000).Wait();
                    return new SaveResultViewModel()
                    {
                        IsSuccess = true,
                        Message = "Данные сохранены успешно",
                        SavedData = new StudentListViewModel()
                        {
                            StudentFullName = student.FullName
                        }
                    };
                }

                return new SaveResultViewModel()
                {
                    IsSuccess = false,
                    Message = "Произошла ошибка при сохранении"
                };
            }
        }

        public SaveResultViewModel RegisterStudent(StudentDetailsViewModel studentDetailsViewModel)
        {
            using (var db = new StudentsAppDatabase())
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {   
                        var firstName = studentDetailsViewModel.FirstName;
                        var surName = studentDetailsViewModel.SurName;
                        var userLogin = firstName + surName;
                        User newUser = User.Create(userLogin, RoleType.Student);
                        db.Users.Add(newUser);
                        if (db.SaveChanges() > 0)
                        {
                            Student studentToRegister = Student.Create(newUser.UserId, firstName, surName);
                            db.Students.Add(studentToRegister);

                            foreach (TeacherViewModel teacherViewModel in studentDetailsViewModel.Teachers)
                            {
                                var teacher = _teacherRepository.GetById(teacherViewModel.TeacherUserId);
                                db.Entry(teacher).State = EntityState.Unchanged;
                                db.StudentMarks.Add(StudentMark.NewStudentAttendsTeacher(studentToRegister, teacher));
                            }

                            if (db.SaveChanges() > 0)
                            {
                                Task.Delay(2000).Wait();
                                dbTransaction.Commit();

                                return new SaveResultViewModel()
                                {
                                    IsSuccess = true,
                                    Message = "Студент зарегистрирован успешно",
                                    SavedData = new StudentListViewModel()
                                    {
                                        StudentUserId = studentToRegister.StudentUserId,
                                        StudentFullName = studentToRegister.FullName
                                    }
                                };
                            }
                        }

                        return new SaveResultViewModel()
                        {
                            IsSuccess = false,
                            Message = "Произошла ошибка при регистрации"
                        };
                    }
                    catch (Exception)
                    {
                        dbTransaction.Rollback();

                        return new SaveResultViewModel()
                        {
                            IsSuccess = false,
                            Message = "Произошла ошибка при регистрации"
                        };
                    }
                }
            }
        }
    }
}