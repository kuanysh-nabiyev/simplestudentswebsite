﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using StudentsApp.Persistance;

namespace StudentsApp.ApplicationServices.Services.Impl
{
    public class TeacherAppService : ITeacherAppService
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IUserRepository _userRepository;

        public TeacherAppService(ITeacherRepository teacherRepository, 
            IStudentRepository studentRepository, 
            IUserRepository userRepository)
        {
            _teacherRepository = teacherRepository;
            _studentRepository = studentRepository;
            _userRepository = userRepository;
        }

        public IEnumerable<TeacherListViewModel> GetAllTeachers()
        {
            using (var db = new StudentsAppDatabase())
            {
                var teachers = 
                    from teacher in db.Teachers
                    select new TeacherListViewModel()
                    {
                        TeacherUserId = teacher.TeacherUserId,
                        FirstName = teacher.FirstName,
                        SurName = teacher.SurName,
                        SubjectName = teacher.SubjectName,
                        QuantityOfStudents = teacher.StudentMarks.Select(a => a.Student.StudentUserId).Distinct().Count()
                    };

                return teachers.ToList();
            }
        }

        public IEnumerable<TeacherListViewModel> GetTeachersWhoHaveLeastQuantityOfStudents()
        {
            var allTeachers = GetAllTeachers().ToList();
            int leastQuantityOfStudents = allTeachers.Min(a => a.QuantityOfStudents);

            var result = allTeachers.Where(a => a.QuantityOfStudents == leastQuantityOfStudents).ToList();

            return result;
        }

        public IEnumerable<TeacherListViewModel> GetTeachersWhoStudyAllStudents()
        {
            var allTeachers = GetAllTeachers().ToList();
            int quantityOfAllStudents = _studentRepository.QuantityOfAllStudents();

            var result = allTeachers.Where(a => a.QuantityOfStudents == quantityOfAllStudents).ToList();

            return result;
        }

        public IEnumerable<TeacherViewModel> GetAllTeachersExceptStudentTeachers(int studentId)
        {
            var teachers = _teacherRepository.GetAll();

            if (studentId != 0)
            {
                Student student = _studentRepository.GetDetailsById(studentId);
                teachers = teachers.Except(student.Teachers).ToList();
            }

            return teachers.Select(teacher => new TeacherViewModel()
            {
                TeacherUserId = teacher.TeacherUserId,
                FirstName = teacher.FirstName,
                SurName = teacher.SurName
            });
        }

        public TeacherDetailsViewModel GetTeacherDetails(int teacherUserId)
        {
            Teacher teacher = _teacherRepository.GetDetailsById(teacherUserId);
            if (teacher == null)
            {
                throw new Exception(String.Format("Преподаватель {0} не найден в БД", teacherUserId));
            }

            teacher.User = _userRepository.GetById(teacherUserId);

            IEnumerable<StudentViewModel> teacherStudents = teacher.StudentMarks.Select(mark =>
                new StudentViewModel()
                {
                    FirstName = mark.Student.FirstName,
                    SurName = mark.Student.SurName,
                    StudentUserId = mark.Student.StudentUserId
                });

            return new TeacherDetailsViewModel()
            {
                TeacherUserId = teacher.TeacherUserId,
                Login = teacher.User.Login,
                FirstName = teacher.FirstName,
                SurName = teacher.SurName,
                SubjectName = teacher.SubjectName,
                Students = teacherStudents.ToList()
            };
        }

        public SaveResultViewModel SaveTeacher(TeacherDetailsViewModel teacherDetailsViewModel)
        {
            using (var db = new StudentsAppDatabase())
            {
                var teacher = _teacherRepository.GetDetailsById(teacherDetailsViewModel.TeacherUserId);
                db.Entry(teacher).State = EntityState.Modified;

                teacher.UpdateProfile(teacherDetailsViewModel.FirstName, 
                    teacherDetailsViewModel.SurName, teacherDetailsViewModel.SubjectName);

                foreach (StudentViewModel studentViewModel in teacherDetailsViewModel.Students)
                {
                    bool isNewStudentToTeacher =
                        teacher.StudentMarks.FirstOrDefault(a => a.Student.StudentUserId == studentViewModel.StudentUserId) ==
                        null;

                    if (isNewStudentToTeacher)
                    {
                        var student = _studentRepository.GetById(studentViewModel.StudentUserId);
                        db.Entry(student).State = EntityState.Unchanged;
                        db.StudentMarks.Add(StudentMark.NewStudentAttendsTeacher(student, teacher));
                    }
                }

                if (db.SaveChanges() > 0)
                {
                    return new SaveResultViewModel()
                    {
                        IsSuccess = true,
                        Message = "Данные сохранены успешно",
                        SavedData = new TeacherListViewModel()
                        {
                            TeacherUserId = teacher.TeacherUserId,
                            FirstName = teacher.FirstName,
                            SurName = teacher.SurName,
                            SubjectName = teacher.SubjectName,
                            QuantityOfStudents = teacher.Students.Count()
                        }
                    };
                }

                return new SaveResultViewModel()
                {
                    IsSuccess = false,
                    Message = "Произошла ошибка при сохранении"
                };
            }
        }

        public SaveResultViewModel RegisterTeacher(TeacherDetailsViewModel teacherDetailsViewModel)
        {
            using (var db = new StudentsAppDatabase())
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var firstName = teacherDetailsViewModel.FirstName;
                        var surName = teacherDetailsViewModel.SurName;
                        var userLogin = firstName + surName;
                        User newUser = User.Create(userLogin, RoleType.Teacher);
                        db.Users.Add(newUser);
                        if (db.SaveChanges() > 0)
                        {
                            Teacher teacherToRegister = 
                                Teacher.Create(newUser.UserId, firstName, surName, teacherDetailsViewModel.SubjectName);
                            db.Teachers.Add(teacherToRegister);

                            foreach (StudentViewModel studentViewModel in teacherDetailsViewModel.Students)
                            {
                                var student = _studentRepository.GetById(studentViewModel.StudentUserId);
                                db.Entry(student).State = EntityState.Unchanged;
                                db.StudentMarks.Add(StudentMark.NewStudentAttendsTeacher(student, teacherToRegister));
                            }

                            if (db.SaveChanges() > 0)
                            {
                                dbTransaction.Commit();

                                return new SaveResultViewModel()
                                {
                                    IsSuccess = true,
                                    Message = "Преподаватель зарегистрирован успешно",
                                    SavedData = new TeacherListViewModel()
                                    {
                                        TeacherUserId = teacherToRegister.TeacherUserId,
                                        FirstName = teacherToRegister.FirstName,
                                        SurName = teacherToRegister.SurName,
                                        SubjectName = teacherToRegister.SubjectName,
                                        QuantityOfStudents = teacherDetailsViewModel.Students.Count
                                    }
                                };
                            }
                        }

                        return new SaveResultViewModel()
                        {
                            IsSuccess = false,
                            Message = "Произошла ошибка при регистрации"
                        };
                    }
                    catch (Exception)
                    {
                        dbTransaction.Rollback();

                        return new SaveResultViewModel()
                        {
                            IsSuccess = false,
                            Message = "Произошла ошибка при регистрации"
                        };
                    }
                }
            }
        }
    }
}