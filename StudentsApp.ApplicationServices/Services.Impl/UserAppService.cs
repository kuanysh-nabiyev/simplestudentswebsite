﻿using System.Linq;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;
using StudentsApp.Persistance;

namespace StudentsApp.ApplicationServices.Services.Impl
{
    public class UserAppService : IUserAppService
    {
        public bool IsUserExists(UserViewModel userViewModel, out int userId)
        {
            User user = GetUserByLoginAndPassword(userViewModel.Login, userViewModel.Password);
            if (user != null)
            {
                userId = user.UserId;
                return true;
            }

            userId = 0;
            return false;
        }

        public User GetUserByLoginAndPassword(string login, string password)
        {
            using (var db = new StudentsAppDatabase())
            {
                User user = db.Users.FirstOrDefault(a => a.Login == login && a.Password == password);

                return user;
            }
        }

        public bool HasPermission(int userId, RoleType roleType)
        {
            using (var db = new StudentsAppDatabase())
            {
                User user = db.Users.Find(userId);

                if (user.RoleType == RoleType.Dean)
                {
                    return true;
                }
                
                if (user.RoleType == roleType)
                {
                    return true;
                }

                return false;
            }
        }
    }
}