﻿using System.Collections.Generic;
using StudentsApp.ApplicationServices.ViewModels;

namespace StudentsApp.ApplicationServices.Services
{
    public interface IStudentAppService
    {
        IEnumerable<StudentListViewModel> GetAllStudents();
        IEnumerable<StudentListViewModel> GetStudentsWhoHaveGreaterThanAverageMark();
        StudentDetailsViewModel GetStudentDetails(int studentUserId);
        SaveResultViewModel SaveStudent(StudentDetailsViewModel studentDetailsViewModel);
        SaveResultViewModel RegisterStudent(StudentDetailsViewModel studentViewModel);
    }
}