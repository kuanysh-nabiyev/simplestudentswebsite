﻿using System.Collections.Generic;
using StudentsApp.ApplicationServices.ViewModels;

namespace StudentsApp.ApplicationServices.Services
{
    public interface ITeacherAppService
    {
        IEnumerable<TeacherListViewModel> GetAllTeachers();
        IEnumerable<TeacherListViewModel> GetTeachersWhoHaveLeastQuantityOfStudents();
        IEnumerable<TeacherListViewModel> GetTeachersWhoStudyAllStudents();
    }
}