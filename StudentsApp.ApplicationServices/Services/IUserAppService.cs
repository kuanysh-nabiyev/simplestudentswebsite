using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.ApplicationServices.Services
{
    public interface IUserAppService
    {
        bool IsUserExists(UserViewModel userViewModel, out int userId);
        bool HasPermission(int userId, RoleType roleType);
    }
}