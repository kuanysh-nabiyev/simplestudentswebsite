﻿namespace StudentsApp.ApplicationServices.ViewModels
{
    public class SaveResultViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public dynamic SavedData { get; set; }
    }
}