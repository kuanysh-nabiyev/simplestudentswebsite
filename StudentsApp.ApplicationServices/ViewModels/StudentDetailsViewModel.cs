﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class StudentDetailsViewModel
    {
        public StudentDetailsViewModel()
        {
            Teachers = Enumerable.Empty<TeacherViewModel>().ToList();
        }

        [Key]
        [DisplayName("Идентификатор")]
        public int StudentUserId { get; set; }

        [ReadOnly(true)]
        public string Login { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Фамилия")]
        public string SurName { get; set; }

        [DisplayName("Учителя")]
        public IList<TeacherViewModel> Teachers { get; set; }

    }
}