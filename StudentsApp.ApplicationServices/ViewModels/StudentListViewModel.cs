﻿using StudentsApp.DomainModel.Entities;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class StudentListViewModel
    {
        public int StudentUserId { get; set; }
        public string StudentFullName { get; set; }
        public double AverageMark { get; set; }
    }
}