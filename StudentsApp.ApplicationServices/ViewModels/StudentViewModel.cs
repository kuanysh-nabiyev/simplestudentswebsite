using System;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class StudentViewModel
    {
        public int StudentUserId { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", FirstName, SurName); }
        }
    }
}