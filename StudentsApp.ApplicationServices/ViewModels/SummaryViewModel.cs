﻿using System;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class SummaryViewModel
    {
        public int NumberOfStudents { get; set; }
        public int NumberOfTeachers { get; set; }
        public string CurrentDateTime { get; set; }
    }
}