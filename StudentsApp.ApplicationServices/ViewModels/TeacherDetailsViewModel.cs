﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class TeacherDetailsViewModel
    {
        public TeacherDetailsViewModel()
        {
            Students = Enumerable.Empty<StudentViewModel>().ToList();
        }

        [Key]
        [DisplayName("Идентификатор")]
        public int TeacherUserId { get; set; }

        [DisplayName("Логин")]
        public string Login { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Фамилия")]
        public string SurName { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Предмет")]
        public string SubjectName { get; set; }

        [DisplayName("Студенты")]
        public IList<StudentViewModel> Students { get; set; }

    }
}