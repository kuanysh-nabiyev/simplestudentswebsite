﻿namespace StudentsApp.ApplicationServices.ViewModels
{
    public class TeacherListViewModel
    {
        public int TeacherUserId { get; set; }
        public string FirstName { get; set; }
        public string SubjectName { get; set; }
        public int QuantityOfStudents { get; set; }
        public string SurName { get; set; }
    }
}