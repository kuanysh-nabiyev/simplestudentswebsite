using System;
using System.Security.AccessControl;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class TeacherViewModel
    {
        public int TeacherUserId { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", SurName, FirstName); }
        }
    }
}