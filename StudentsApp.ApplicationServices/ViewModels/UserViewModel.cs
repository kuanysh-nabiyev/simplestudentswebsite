﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StudentsApp.ApplicationServices.ViewModels
{
    public class UserViewModel
    {
        [Required]
        [DisplayName("Логин")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Пароль")]
        public string Password { get; set; }
    }
}