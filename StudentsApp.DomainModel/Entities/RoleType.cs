namespace StudentsApp.DomainModel.Entities
{
    public enum RoleType
    {
        Student = 1,
        Teacher,
        Dean
    }
}