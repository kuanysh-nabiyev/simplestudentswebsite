﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentsApp.DomainModel.Entities
{
    public class Student
    {
        // To populate sample data to DB
        public Student(int studentUserId, String firstName, String surName)
        {
            StudentUserId = studentUserId;
            FirstName = firstName;
            SurName = surName;
        }

        /// <summary>
        /// For ORM
        /// </summary>
        protected Student()
        {
        }

        public int StudentUserId { get; private set; }
        public User User { get; set; }
        public string FirstName { get; private set; }
        public string SurName { get; private set; }
        public ICollection<StudentMark> Marks { get; private set; }

        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName, SurName);
            }
        }

        public IEnumerable<Teacher> Teachers
        {
            get
            {
                if (Marks != null)
                {
                    return Marks.Select(a => a.Teacher);
                }

                return Enumerable.Empty<Teacher>();
            }
        }

        public void UpdateProfile(String firstName, String surName)
        {
            FirstName = firstName;
            SurName = surName;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (Student)obj;
            if (StudentUserId == other.StudentUserId)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return StudentUserId.GetHashCode();
        }

        public static Student Create(int studentUserId, string firstName, string surName)
        {
            return new Student()
            {
                StudentUserId = studentUserId,
                FirstName = firstName,
                SurName = surName,
            };
        }
    }
}