﻿namespace StudentsApp.DomainModel.Entities
{
    public class StudentMark
    {
        //to populate sample data
        public StudentMark(int studentMarkId, Student student, Teacher teacher, double mark)
        {
            StudentMarkId = studentMarkId;
            Student = student;
            Teacher = teacher;
            Mark = mark;
        }

        /// <summary>
        /// ORM
        /// </summary>
        protected StudentMark()
        {

        }

        public int StudentMarkId { get; private set; }
        public Student Student { get; private set; }
        public Teacher Teacher { get; private set; }
        public double? Mark { get; private set; }

        public static StudentMark NewStudentAttendsTeacher(Student student, Teacher teacher)
        {
            return new StudentMark()
            {
                Student = student,
                Teacher = teacher
            };
        }
    }
}