using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentsApp.DomainModel.Entities
{
    public class Teacher
    {
        // To populate sample data to DB
        public Teacher(int teacherUserId, String firstName, String surName, string subjectName)
        {
            TeacherUserId = teacherUserId;
            FirstName = firstName;
            SurName = surName;
            SubjectName = subjectName;
        }

        /// <summary>
        /// For ORM
        /// </summary>
        protected Teacher()
        {
        }

        public int TeacherUserId { get; private set; }
        public string FirstName { get; private set; }
        public string SurName { get; private set; }
        public string SubjectName { get; private set; }
        public ICollection<StudentMark> StudentMarks { get; private set; }

        public IEnumerable<Student> Students {
            get { return StudentMarks.Select(a => a.Student); }
        }

        public User User { get; set; }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (Teacher)obj;
            if (TeacherUserId == other.TeacherUserId)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return TeacherUserId.GetHashCode();
        }

        public void UpdateProfile(string firstName, string surName, string subjectName)
        {
            FirstName = firstName;
            SurName = surName;
            SubjectName = subjectName;
        }

        public static Teacher Create(int teacherUserId, string firstName, string surName, string subjectName)
        {
            return new Teacher()
            {
                TeacherUserId = teacherUserId,
                FirstName = firstName,
                SurName = surName,
                SubjectName = subjectName
            };
        }

        public void NewFunction2() {

        }
    }
}