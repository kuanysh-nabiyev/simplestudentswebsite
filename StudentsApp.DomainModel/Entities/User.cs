﻿namespace StudentsApp.DomainModel.Entities
{
    public class User
    {
        //to populate sample data
        public User(int userId, string login, string password, RoleType roleType)
        {
            UserId = userId;
            Login = login;
            Password = password;
            RoleType = roleType;
        }

        /// <summary>
        /// For ORM
        /// </summary>
        protected User()
        {
        }

        public int UserId { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public RoleType RoleType { get; private set; }

        public static User Create(string login, RoleType roleType)
        {
            return new User()
            {
                Login = login,
                Password = "123",
                RoleType = roleType,
            };
        }
    }
}