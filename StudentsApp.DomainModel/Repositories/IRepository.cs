﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentsApp.DomainModel.Repositories
{
    public interface IRepository<TAggregate>
    {
        TAggregate GetById(int id);
        IEnumerable<TAggregate> GetAll();
        IEnumerable<TAggregate> Find(Func<TAggregate, bool> predicate);
        TAggregate GetDetailsById(int id);
    }
}