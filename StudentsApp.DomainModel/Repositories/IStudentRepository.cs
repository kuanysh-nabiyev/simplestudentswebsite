﻿using StudentsApp.DomainModel.Entities;

namespace StudentsApp.DomainModel.Repositories
{
    public interface IStudentRepository : IRepository<Student>
    {
        int QuantityOfAllStudents();
    }
}