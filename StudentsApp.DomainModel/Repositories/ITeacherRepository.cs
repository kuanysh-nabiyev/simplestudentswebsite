﻿using StudentsApp.DomainModel.Entities;

namespace StudentsApp.DomainModel.Repositories
{
    public interface ITeacherRepository : IRepository<Teacher>
    {
        int QuantityOfAllTeachers();
    }
}