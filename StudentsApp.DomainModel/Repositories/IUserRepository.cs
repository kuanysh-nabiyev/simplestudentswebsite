﻿using StudentsApp.DomainModel.Entities;

namespace StudentsApp.DomainModel.Repositories
{
    public interface IUserRepository
    {
        User GetById(int id);
    }
}