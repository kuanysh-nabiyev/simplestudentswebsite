﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.Persistance
{
    public class DbSampleDataInitializer : DropCreateDatabaseAlways<StudentsAppDatabase>
    {
        public override void InitializeDatabase(StudentsAppDatabase context)
        {
            context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
                , string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

            base.InitializeDatabase(context);
        }

        protected override void Seed(StudentsAppDatabase context)
        {
            /////////////////////////////////////////////////////////////////
            // Users
            var users = new List<User>
            {
                new User(1, "kuanysh", "123", RoleType.Student),
                new User(2, "dastan", "123", RoleType.Student),
                new User(3, "ivan", "123", RoleType.Teacher),
                new User(4, "jandar", "123", RoleType.Teacher),
                new User(5, "sergey", "123", RoleType.Student),
                new User(6, "gena", "123", RoleType.Dean),
                new User(7, "palina", "123", RoleType.Student),
                new User(8, "anna", "123", RoleType.Student),
                new User(9, "irina", "123", RoleType.Student),
                new User(10, "sergey", "123", RoleType.Student),
                new User(11, "max", "123", RoleType.Student),
                new User(12, "yarik", "123", RoleType.Student),
                new User(13, "andrey", "123", RoleType.Student),
                new User(14, "alexey", "123", RoleType.Student),
                new User(15, "dima", "123", RoleType.Student),
                new User(16, "vlad", "123", RoleType.Student),
                new User(17, "mary", "123", RoleType.Student),
                new User(18, "alexandr", "123", RoleType.Student),
                new User(19, "viktor", "123", RoleType.Student),
                new User(20, "dimaSokol", "123", RoleType.Student),
                new User(21, "kostya", "123", RoleType.Student),
                new User(22, "fanis", "123", RoleType.Student),
                new User(23, "anton", "123", RoleType.Student),
                new User(24, "viktorKaa", "123", RoleType.Student),
                new User(25, "denis", "123", RoleType.Student),
                new User(26, "vadim", "123", RoleType.Student),
                new User(27, "alexTana", "123", RoleType.Student),
                new User(28, "arsen", "123", RoleType.Teacher),
                new User(29, "lui", "123", RoleType.Teacher),
            };

            /////////////////////////////////////////////////////////////////
            // Students
            var students = new List<Student>
            {
                new Student(1, "Kuanysh", "Nabiyev"),
                new Student(2, "Dastan", "Kabylbekov"),  
                new Student(5, "Sergey", "Mutko"),
                new Student(7, "Полина", "Данилина"),
                new Student(8, "Анна", "Колмакова"),
                new Student(9, "Ирина", "Жигульская"),
                new Student(10, "Сергей", "Терешков"),
                new Student(11, "Максим", "Липовик"),
                new Student(12, "Ярослав", "Чуйков"),
                new Student(13, "Андрей", "Бобрышов"),
                new Student(14, "Алексей", "Минжуков"),
                new Student(15, "Дмитрий", "Гололобов"),
                new Student(16, "Владислав", "Николаев"),
                new Student(17, "Мария", "Таманаева"),
                new Student(18, "Александр", "Бондаренко"),
                new Student(19, "Виктор", "Журавлев"),
                new Student(20, "Дмитрий", "Сокольцов"),
                new Student(21, "Константин", "Добецкий"),
                new Student(22, "Фанис", "Ганиев"),
                new Student(23, "Антон", "Мясников"),
                new Student(24, "Виктор", "Каа"),
                new Student(25, "Денис", "Назаров"),
                new Student(26, "Вадим", "Черемисин"),
                new Student(27, "Александр", "Танаев"),
            };

            // Teacher
            var teachers = new List<Teacher>
            {
                new Teacher(3, "Ivan", "Ivanov", "Math"),
                new Teacher(4, "Jandarbek", "Berik", "History"),
                new Teacher(28, "Arsen", "Venger", "Economy"),
                new Teacher(29, "Lui", "VanGal", "Phylosophy")
            };

            // StudentMarks
            var studentMarks = new List<StudentMark>
            {
                new StudentMark(1, students[0], teachers[0], 5),
                new StudentMark(2, students[0], teachers[1], 4),
                new StudentMark(3, students[1], teachers[0], 2),
                new StudentMark(4, students[1], teachers[1], 4.5),
                new StudentMark(5, students[2], teachers[0], 4.3),
                new StudentMark(6, students[2], teachers[1], 3),
                new StudentMark(7, students[5], teachers[2], 4.3),
                new StudentMark(7, students[5], teachers[2], 4.3),
                new StudentMark(8, students[0], teachers[3], 1),
                new StudentMark(8, students[1], teachers[3], 1),
                new StudentMark(8, students[2], teachers[3], 4),
                new StudentMark(8, students[3], teachers[3], 4),
                new StudentMark(8, students[4], teachers[3], 4),
                new StudentMark(8, students[5], teachers[3], 4),
                new StudentMark(8, students[6], teachers[3], 1),
                new StudentMark(8, students[7], teachers[3], 2),
                new StudentMark(8, students[8], teachers[3], 3),
                new StudentMark(8, students[9], teachers[3], 4),
                new StudentMark(8, students[10], teachers[3], 5),
                new StudentMark(8, students[11], teachers[3], 1),
                new StudentMark(8, students[12], teachers[3], 2),
                new StudentMark(8, students[13], teachers[3], 3),
                new StudentMark(8, students[14], teachers[3], 4),
                new StudentMark(8, students[15], teachers[3], 5),
                new StudentMark(8, students[16], teachers[3], 1),
                new StudentMark(8, students[17], teachers[3], 2),
                new StudentMark(8, students[18], teachers[3], 3),
                new StudentMark(8, students[19], teachers[3], 4),
                new StudentMark(8, students[20], teachers[3], 5),
                new StudentMark(8, students[21], teachers[3], 1),
                new StudentMark(8, students[22], teachers[3], 2),
                new StudentMark(8, students[23], teachers[3], 3),
            };

            context.Users.AddRange(users);
            context.Students.AddRange(students);
            context.Teachers.AddRange(teachers);
            context.StudentMarks.AddRange(studentMarks);

            base.Seed(context);
        }
    }
}
