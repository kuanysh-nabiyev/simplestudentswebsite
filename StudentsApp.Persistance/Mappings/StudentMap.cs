﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.Persistance.Mappings
{
    public class StudentMap : EntityTypeConfiguration<Student>
    {
        internal StudentMap()
        {
            HasKey(t => t.StudentUserId);
            Property(t => t.StudentUserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnName("StudentUserId");

            Property(t => t.FirstName)
                .IsRequired()
                .HasColumnName("FirstName");
            Property(t => t.SurName)
                .IsRequired()
                .HasColumnName("SurName");

            ToTable("Students");
        }
    }
}
