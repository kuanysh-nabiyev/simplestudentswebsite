﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.Persistance.Mappings
{
    public class StudentMarkMap : EntityTypeConfiguration<StudentMark>
    {
        internal StudentMarkMap()
        {
            HasKey(t => t.StudentMarkId);

            HasRequired(t => t.Student)
                .WithMany(student => student.Marks);
            HasRequired(t => t.Teacher)
                .WithMany(teacher => teacher.StudentMarks);
            Property(t => t.Mark)
                .IsOptional()
                .HasColumnName("Mark");

            ToTable("StudentMarks");
        }
    }
}