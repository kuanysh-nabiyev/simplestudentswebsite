﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.Persistance.Mappings
{
    public class TeacherMap : EntityTypeConfiguration<Teacher>
    {
        internal TeacherMap()
        {
            HasKey(t => t.TeacherUserId);
            Property(t => t.TeacherUserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnName("TeacherUserId");

            Property(t => t.FirstName)
                .IsRequired()
                .HasColumnName("FirstName");
            Property(t => t.SurName)
                .IsRequired()
                .HasColumnName("SurName");
            Property(t => t.SubjectName)
                .IsRequired()
                .HasColumnName("SubjectName");
            
            ToTable("Teachers");
        }
    }
}
