﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.Persistance.Mappings
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        internal UserMap()
        {
            HasKey(t => t.UserId);
            //Property(t => t.UserId)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
            //    .HasColumnName("UserId");

            Property(t => t.Login)
                .IsRequired()
                .HasColumnName("Login");
            Property(t => t.Password)
                .IsRequired()
                .HasColumnName("Password");
            Property(t => t.RoleType)
                .IsRequired()
                .HasColumnName("RoleType");

            ToTable("Users");
        }
    }
}
