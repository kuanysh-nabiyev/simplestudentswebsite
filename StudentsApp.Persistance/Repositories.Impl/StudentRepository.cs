﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using System.Data.Entity;

namespace StudentsApp.Persistance.Repositories.Impl
{
    public class StudentRepository : IStudentRepository
    {
        public Student GetDetailsById(int userId)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Students
                    .Include(a => a.Marks.Select(b => b.Teacher))
                    .SingleOrDefault(st => st.StudentUserId == userId);
            }
        }

        public int QuantityOfAllStudents()
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Students.Count();
            }
        }

        public Student GetById(int id)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Students.Find(id);
            }
        }

        public IEnumerable<Student> GetAll()
        {
            return Find(All);
        }

        private bool All(Student student)
        {
            return true;
        }

        public IEnumerable<Student> Find(Func<Student, bool> predicate)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Students.Where(predicate).ToList();
            }
        }


    }
}