﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using System.Data.Entity;

namespace StudentsApp.Persistance.Repositories.Impl
{
    public class TeacherRepository : ITeacherRepository
    {
        public Teacher GetDetailsById(int teacherUserId)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Teachers
                    .Include(a => a.StudentMarks.Select(b => b.Student))
                    .SingleOrDefault(t => t.TeacherUserId == teacherUserId);
            }
        }

        public int QuantityOfAllTeachers()
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Teachers.Count();
            }
        }

        public Teacher GetById(int id)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Teachers.Find(id);
            }
        }

        public IEnumerable<Teacher> GetAll()
        {
            return Find(All);
        }

        private bool All(Teacher teacher)
        {
            return true;
        }

        public IEnumerable<Teacher> Find(Func<Teacher, bool> predicate)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Teachers.Where(predicate).ToList();
            }
        }

    }
}