﻿using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;

namespace StudentsApp.Persistance.Repositories.Impl
{
    public class UserRepository : IUserRepository
    {
        public User GetById(int id)
        {
            using (var db = new StudentsAppDatabase())
            {
                return db.Users.Find(id);
            } 
        }
    }
}