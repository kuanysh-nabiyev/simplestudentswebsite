﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using StudentsApp.DomainModel.Entities;
using StudentsApp.Persistance.Mappings;

namespace StudentsApp.Persistance
{
    public class StudentsAppDatabase: DbContext
    {
        static StudentsAppDatabase()
        {
            Database.SetInitializer(new DbSampleDataInitializer());
        }

        public StudentsAppDatabase()
            : base("StudentsAppDb") 
        {
            Users = base.Set<User>();
            Students = base.Set<Student>();
            StudentMarks = base.Set<StudentMark>();
            Teachers = base.Set<Teacher>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            EnsureDatabase(modelBuilder);
        }

        public DbSet<Student> Students { get; private set; }
        public DbSet<User> Users { get; private set; }
        public DbSet<StudentMark> StudentMarks { get; private set; }
        public DbSet<Teacher> Teachers { get; private set; }

        #region Database modeling
        public static void EnsureDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new StudentMap());
            modelBuilder.Configurations.Add(new TeacherMap());
            modelBuilder.Configurations.Add(new StudentMarkMap());
        }
        #endregion
    }
}