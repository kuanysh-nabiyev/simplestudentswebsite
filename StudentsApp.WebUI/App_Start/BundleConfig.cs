﻿using System.Web;
using System.Web.Optimization;

namespace StudentsApp.WebUI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.validate.unobtrusive*"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/jquery.dataTables.js",
                        "~/Scripts/dataTables.bootstrap.js",
                        "~/Scripts/DataTables-1.10.10/extensions/Select/js/dataTables.select.js",
                        "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/dataTables.buttons.js",
                        "~/Scripts/DataTables-1.10.10/extensions/Buttons/js/buttons.bootstrap.js",
                        "~/Scripts/select2.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/myScripts").Include(
                "~/Scripts/Mine/dataTablesOptions.js",
                "~/Scripts/Mine/menu-tabs.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/jquery.dataTables.css",
                "~/Content/dataTables.bootstrap.css",
                "~/Content/DataTables-1.10.10/extensions/Select/css/select.dataTables.css",
                "~/Content/DataTables-1.10.10/extensions/Select/css/select.bootstrap.css",
                "~/Content/DataTables-1.10.10/extensions/Buttons/css/buttons.dataTables.css",
                "~/Content/DataTables-1.10.10/extensions/Buttons/css/buttons.bootstrap.css",
                "~/Content/select2/select2-bootstrap.css",
                "~/Content/select2/select2.css",
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}