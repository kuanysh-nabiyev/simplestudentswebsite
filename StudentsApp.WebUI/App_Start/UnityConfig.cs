﻿using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using StudentsApp.ApplicationServices.Services;
using StudentsApp.ApplicationServices.Services.Impl;
using StudentsApp.DomainModel.Repositories;
using StudentsApp.Persistance.Repositories.Impl;
using Unity.Mvc5;

namespace StudentsApp.WebUI
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IStudentRepository, StudentRepository>();
            container.RegisterType<ITeacherRepository, TeacherRepository>();
            container.RegisterType<IUserRepository, UserRepository>();

            container.RegisterType<IStudentAppService, StudentAppService>();
            container.RegisterType<ITeacherAppService, TeacherAppService>();
            container.RegisterType<IUserAppService, UserAppService>();
        }
    }
}
