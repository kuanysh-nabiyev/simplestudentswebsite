﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using StudentsApp.ApplicationServices.Services;
using StudentsApp.ApplicationServices.Services.Impl;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserAppService _userAppService;

        public AccountController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        public ActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Login(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                int userId;
                if (_userAppService.IsUserExists(userViewModel, out userId))
                {
                    FormsAuthentication.SetAuthCookie(userId.ToString(), false);

                    return Json(new LoginResultViewModel()
                    {
                        IsSuccess = true,
                        Message = "Добро пожаловать, " + userViewModel.Login
                    });
                }
            }

            return Json(new LoginResultViewModel()
            {
                IsSuccess = false,
                Message = "Произошла ошибка при попытке войти в систему"
            });
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Unauthorized()
        {
            if (Request.IsAjaxRequest())
            {
                if (Request.IsAuthenticated)
                {
                    return Json(new
                    {
                        IsAuthenticated = true,
                        HasPermission = false
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        IsAuthenticated = false,
                        HasPermission = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return View("Unauthorized");
        }
    }
}