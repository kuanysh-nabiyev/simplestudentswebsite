﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Repositories;

namespace StudentsApp.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ITeacherRepository _teacherRepository;

        public HomeController(IStudentRepository studentRepository, 
            ITeacherRepository teacherRepository)
        {
            _studentRepository = studentRepository;
            _teacherRepository = teacherRepository;
        }

        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SummaryInfo()
        {
            return Json(new SummaryViewModel()
            {
                CurrentDateTime = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                NumberOfStudents = _studentRepository.QuantityOfAllStudents(),
                NumberOfTeachers = _teacherRepository.QuantityOfAllTeachers()
            }, JsonRequestBehavior.AllowGet);
        }
    }
}