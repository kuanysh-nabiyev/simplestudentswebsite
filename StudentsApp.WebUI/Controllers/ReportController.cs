﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsApp.ApplicationServices.Services;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using StudentsApp.WebUI.Filters;

namespace StudentsApp.WebUI.Controllers
{
    public class ReportController : Controller
    {
        private readonly IStudentAppService _studentAppService;
        private readonly ITeacherAppService _teacherAppService;

        public ReportController(IStudentAppService studentAppService, ITeacherAppService teacherAppService)
        {
            _studentAppService = studentAppService;
            _teacherAppService = teacherAppService;
        }

        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult TeachersWhoStudyAllStudents()
        {
            var teachers = _teacherAppService.GetTeachersWhoStudyAllStudents();
            return Json(new { data = teachers }, JsonRequestBehavior.AllowGet);
        }

        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult StudentsWhoHaveGreaterThanAverageMark()
        {
            var students = _studentAppService.GetStudentsWhoHaveGreaterThanAverageMark();
            return Json(new { data = students}, JsonRequestBehavior.AllowGet);
        }

        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult TeachersWhoHaveLeastQuantityOfStudents()
        {
            var teachers = _teacherAppService.GetTeachersWhoHaveLeastQuantityOfStudents();
            return Json(new { data = teachers }, JsonRequestBehavior.AllowGet);
        }
    }
}
