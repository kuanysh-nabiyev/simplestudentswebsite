﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using StudentsApp.ApplicationServices.Services;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;
using StudentsApp.DomainModel.Repositories;
using StudentsApp.WebUI.Filters;

namespace StudentsApp.WebUI.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentAppService _studentAppService;
        private readonly IStudentRepository _studentRepository;

        public StudentController(IStudentAppService studentAppService,
            IStudentRepository studentRepository)
        {
            _studentAppService = studentAppService;
            _studentRepository = studentRepository;
        }

        public ActionResult List()
        {
            var students = _studentAppService.GetAllStudents();
            return new JsonResult
            {
                Data = new
                {
                    data = students
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //
        // GET: /Student/Details/5
        [IsAuthorized(RoleType = RoleType.Student)]
        public ActionResult Details(int id)
        {
            StudentDetailsViewModel studentDetails = _studentAppService.GetStudentDetails(id);

            return PartialView(studentDetails);
        }

        //
        // GET: /Student/Create
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Create()
        {
            return PartialView(new StudentDetailsViewModel());
        }

        //
        // POST: /Student/Create
        [HttpPost]
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Create(StudentDetailsViewModel studentViewModel)
        {
            if (ModelState.IsValid)
            {
                SaveResultViewModel saveResult = _studentAppService.RegisterStudent(studentViewModel);

                return Json(saveResult);
            }

            return Json(new SaveResultViewModel()
            {
                IsSuccess = false,
                Message = "Введенные данные не корректны"
            });
        }

        //
        // GET: /Student/Edit/5
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Edit(int id)
        {
            StudentDetailsViewModel studentDetails = _studentAppService.GetStudentDetails(id);

            return PartialView(studentDetails);
        }

        //
        // POST: /Student/Edit/5
        [HttpPost]
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Edit(StudentDetailsViewModel studentViewModel)
        {
            if (ModelState.IsValid)
            {
                SaveResultViewModel saveResult = _studentAppService.SaveStudent(studentViewModel);

                return Json(saveResult);
            }

            return Json(new SaveResultViewModel()
            {
                IsSuccess = false,
                Message = "Введенные данные не корректны"
            });
        }

    }
}
