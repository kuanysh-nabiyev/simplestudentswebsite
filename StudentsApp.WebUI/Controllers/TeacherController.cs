﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentsApp.ApplicationServices.Services.Impl;
using StudentsApp.ApplicationServices.ViewModels;
using StudentsApp.DomainModel.Entities;
using StudentsApp.WebUI.Filters;

namespace StudentsApp.WebUI.Controllers
{
    public class TeacherController : Controller
    {
        private readonly TeacherAppService _teacherAppService;

        public TeacherController(TeacherAppService teacherAppService)
        {
            _teacherAppService = teacherAppService;
        }

        public ActionResult List()
        {
            var teachers = _teacherAppService.GetAllTeachers();
            return new JsonResult
            {
                Data = new
                {
                    data = teachers
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult TeachersThatCanBeAdded(int studentId)
        {
            IEnumerable<TeacherViewModel> teachers = 
                _teacherAppService.GetAllTeachersExceptStudentTeachers(studentId);

            return new JsonResult
            {
                Data = new
                {
                    items = teachers
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //
        // GET: /Teacher/Details/5
        [IsAuthorized(RoleType = RoleType.Teacher)]
        public ActionResult Details(int id)
        {
            TeacherDetailsViewModel teacherDetails = _teacherAppService.GetTeacherDetails(id);

            return PartialView(teacherDetails);
        }

        //
        // GET: /Teacher/Create
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Create()
        {
            return PartialView(new TeacherDetailsViewModel());
        }

        //
        // POST: /Teacher/Create
        [HttpPost]
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Create(TeacherDetailsViewModel teacherDetailsViewModel)
        {
            if (ModelState.IsValid)
            {
                SaveResultViewModel saveResult = _teacherAppService.RegisterTeacher(teacherDetailsViewModel);

                return Json(saveResult);
            }

            return Json(new SaveResultViewModel()
            {
                IsSuccess = false,
                Message = "Введенные данные не корректны"
            });
        }

        //
        // GET: /Teacher/Edit/5
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Edit(int id)
        {
            TeacherDetailsViewModel teacherDetails = _teacherAppService.GetTeacherDetails(id);

            return PartialView(teacherDetails);
        }

        //
        // POST: /Teacher/Edit/5
        [HttpPost]
        [IsAuthorized(RoleType = RoleType.Dean)]
        public ActionResult Edit(TeacherDetailsViewModel teacherDetailsViewModel)
        {
            if (ModelState.IsValid)
            {
                SaveResultViewModel saveResult = _teacherAppService.SaveTeacher(teacherDetailsViewModel);

                return Json(saveResult);
            }

            return Json(new SaveResultViewModel()
            {
                IsSuccess = false,
                Message = "Введенные данные не корректны"
            });
        }
    }
}
