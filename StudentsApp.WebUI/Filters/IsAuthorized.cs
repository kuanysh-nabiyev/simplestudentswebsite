﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using StudentsApp.ApplicationServices.Services;
using StudentsApp.DomainModel.Entities;

namespace StudentsApp.WebUI.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class IsAuthorized : AuthorizeAttribute
    {
        private readonly IUserAppService _userAppService;

        public IsAuthorized()
            : this(DependencyResolver.Current.GetService<IUserAppService>())
        {
        }

        public IsAuthorized(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        public RoleType RoleType { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }

            int userId;
            if (int.TryParse(httpContext.User.Identity.Name, out userId))
            {
                if (_userAppService.HasPermission(userId, RoleType))
                {
                    return true;
                }
            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "Account",
                                action = "Unauthorized"
                            })
                        );
        }
    }
}