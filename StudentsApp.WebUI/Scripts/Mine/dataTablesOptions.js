﻿function DtOptions() {
    var translationsUrl = "//cdn.datatables.net/plug-ins/1.10.9/i18n/Russian.json";

    var teacherColumns = [
            { "data": "FirstName" },
            { "data": "SubjectName" },
            { "data": "QuantityOfStudents" }
    ];
    var studentColumns = [
        { "data": "StudentUserId" },
        { "data": "StudentFullName" },
        { "data": "AverageMark" }
    ];

    this.TeacherColumns = function () {
        return teacherColumns;
    }

    this.StudentColumns = function () {
        return studentColumns;
    }

    this.CanCreateDataTable = function (tableId) {
        if ($.fn.DataTable.isDataTable(tableId)) {
            return false;
        }

        return true;
    }

    this.GetReportOptions = function (ajaxUrl, aColumns) {
        return {
            "lengthMenu": [[-1], ['Все']],
            "bLengthChange": false,
            "language": {
                "url": translationsUrl
            },
            columns: aColumns,
            "bProcessing": true,
            "bServerSide": true,
            fnServerData: function (url, data, callback) {
                $.ajax({
                    url: ajaxUrl,
                    success: function (result) {
                        window.CheckPermission(result, function() {
                            callback(result);
                        });
                    },               
                    error: function (error) {
                        console.log(error);
                    }
                });
            },
        }
    }

    this.GetStudentListOptions = function () {
        var controllerName = "/Student";

        return {
            dom: 'Bfrtip',
            "lengthMenu": [[20], [20]],
            "bLengthChange": false,
            "language": {
                "url": translationsUrl
            },
            select: true,
            ajax: controllerName + '/List',
            columns: studentColumns,
            buttons: [
                {
                    text: 'Регистрация студента',
                    action: function (e, dt, node, config) {
                        var createStudentModalId = 'modal-create-student';
                        $.ajax({
                            url: controllerName + '/Create',
                            success: function (result) {
                                window.CheckPermission(result, function () {
                                    window.ShowDialog(createStudentModalId, result);
                                });
                            }
                        });

                        $('#btn-create-student').off().one('click', function (ev) {
                            var form = $('#form-create-student');
                            var $loading = $('#' + createStudentModalId).find('.dialog-loading-div').show();
                            $.ajax({
                                url: form.attr('action'),
                                method: 'POST',
                                data: form.serialize(),
                                success: function (result) {
                                    if (result.IsSuccess) {
                                        dt.row.add(result.SavedData).draw();
                                        window.CloseDialog(createStudentModalId);
                                    }
                                    window.ShowMessage(result.Message);
                                },
                                complete: function () {
                                    $loading.hide();
                                }
                            });
                        });
                    }
                },
                {
                    extent: 'selected',
                    text: 'Просмотреть детали',
                    action: function (e, dt, node, config) {
                        var selectedRowData = dt.row({ selected: true }).data();
                        if (selectedRowData != undefined) {
                            $.ajax({
                                url: controllerName + '/Details',
                                data: { id: selectedRowData.StudentUserId },
                                success: function (result) {
                                    window.CheckPermission(result, function () {
                                        $(result).modal('show');
                                    });
                                }
                            });
                        } else {
                            window.ShowMessage("Необходимо выбрать студента");
                        }
                    }
                },
                {
                    text: 'Редактировать',
                    action: function (e, dt, node, config) {
                        var editStudentModalId = 'modal-edit-student';
                        var selectedRow = dt.row({ selected: true });
                        var selectedRowData = selectedRow.data();
                        if (selectedRowData != undefined) {
                            $.ajax({
                                url: controllerName + '/Edit',
                                data: { id: selectedRowData.StudentUserId },
                                success: function (result) {
                                    window.CheckPermission(result, function () {
                                        window.ShowDialog(editStudentModalId, result);
                                    });
                                }
                            });
                            $('#btn-save-student').off().one('click', function (ev) {
                                var form = $('#form-edit-student');
                                var $loading = $('#' + editStudentModalId).find('.dialog-loading-div').show();

                                $.ajax({
                                    url: form.attr('action'),
                                    method: 'POST',
                                    data: form.serialize(),
                                    success: function (result) {
                                        if (result.IsSuccess) {
                                            selectedRowData.StudentFullName = result.SavedData.StudentFullName;
                                            selectedRow.data(selectedRowData).draw();
                                            window.CloseDialog(editStudentModalId);
                                        }
                                        window.ShowMessage(result.Message);
                                    },
                                    complete: function () {
                                        $loading.hide();
                                    }
                                });
                            });
                        } else {
                            window.ShowMessage("Необходимо выбрать студента");
                        }
                    }
                }
            ]
        }
    };

    this.GetTeacherListOptions = function () {
        var controllerName = "/Teacher";
        return {
            dom: 'Bfrtip',
            "lengthMenu": [[20], [20]],
            "bLengthChange": false,
            "language": {
                "url": translationsUrl
            },
            select: true,
            ajax: controllerName + '/List',
            columns: teacherColumns,
            buttons: [
                {
                    text: 'Регистрация преподавателя',
                    action: function (e, dt, node, config) {
                        var createTeacherModalId = 'modal-create-teacher';
                        $.ajax({
                            url: controllerName + '/Create',
                            success: function (result) {
                                window.CheckPermission(result, function() {
                                    window.ShowDialog(createTeacherModalId, result);
                                });
                            }
                        });

                        $('#btn-create-teacher').off().one('click', function (ev) {
                            var form = $('#form-create-teacher');
                            var $loading = $('#' + createTeacherModalId).find('.dialog-loading-div').show();
                            $.ajax({
                                url: form.attr('action'),
                                method: 'POST',
                                data: form.serialize(),
                                success: function (result) {
                                    if (result.IsSuccess) {
                                        dt.row.add(result.SavedData).draw();
                                        window.CloseDialog(createTeacherModalId);
                                    }
                                    window.ShowMessage(result.Message);
                                },
                                complete: function () {
                                    $loading.hide();
                                }
                            });
                        });
                    }
                },
                {
                    extent: 'selected',
                    text: 'Просмотреть детали',
                    action: function (e, dt, node, config) {
                        var selectedRowData = dt.row({ selected: true }).data();
                        if (selectedRowData != undefined) {
                            $.ajax({
                                url: controllerName + '/Details',
                                data: { id: selectedRowData.TeacherUserId },
                                success: function (result) {
                                    window.CheckPermission(result, function() {
                                        $(result).modal('show');
                                    });
                                }
                            });
                        } else {
                            window.ShowMessage("Необходимо выбрать преподавателя");
                        }
                    }
                },
                {
                    text: 'Редактировать',
                    action: function (e, dt, node, config) {
                        var editTeacherModalId = 'modal-edit-teacher';
                        var selectedRow = dt.row({ selected: true });
                        var selectedRowData = selectedRow.data();
                        if (selectedRowData != undefined) {
                            $.ajax({
                                url: controllerName + '/Edit',
                                data: { id: selectedRowData.TeacherUserId },
                                success: function (result) {
                                    window.CheckPermission(result, function() {
                                        window.ShowDialog(editTeacherModalId, result);
                                    });
                                }
                            });
                            $('#btn-save-teacher').off().one('click', function (ev) {
                                var form = $('#form-edit-teacher');
                                var $loading = $('#' + editTeacherModalId).find('.dialog-loading-div').show();

                                $.ajax({
                                    url: form.attr('action'),
                                    method: 'POST',
                                    data: form.serialize(),
                                    success: function (result) {
                                        if (result.IsSuccess) {
                                            selectedRow.data(result.SavedData).draw();
                                            window.CloseDialog(editTeacherModalId);
                                        }
                                        window.ShowMessage(result.Message);
                                    },
                                    complete: function () {
                                        $loading.hide();
                                    }
                                });
                            });
                        } else {
                            window.ShowMessage("Необходимо выбрать преподавателя");
                        }
                    }
                }
            ]
        };
    }
}