﻿$(document).ready(function () {
    var dtOptions = new DtOptions();

    $('.nav a[href="#students"]').click(function (e) {
        var tableId = '#studentsTable';
        if (dtOptions.CanCreateDataTable(tableId)) {
            $('#studentsTable').DataTable(dtOptions.GetStudentListOptions());
        }
        showTab(this);
    });

    $('.nav a[href="#teachers"]').click(function (e) {
        var tableId = '#teachersTable';
        if (dtOptions.CanCreateDataTable(tableId)) {
            $(tableId).DataTable(dtOptions.GetTeacherListOptions());
        }
        showTab(this);
    });

    var controllerName = '/Report';

    $('.nav a[href="#teachersWhoStudyAllStudents"]').click(function (e) {
        var tableId = '#teachersWhoStudyAllStudentsTable';
        var ajaxUrl = controllerName + "/TeachersWhoStudyAllStudents";
        if (dtOptions.CanCreateDataTable(tableId)) {
            $(tableId).DataTable(dtOptions.GetReportOptions(ajaxUrl, dtOptions.TeacherColumns()));
        }

        showTab(this);
    });

    $('.nav a[href="#studentsWhoHaveGreaterThanAverageMark"]').click(function (e) {
        var tableId = '#StudentsWhoHaveGreaterThanAverageMarkTable';
        var ajaxUrl = controllerName + "/StudentsWhoHaveGreaterThanAverageMark";
        if (dtOptions.CanCreateDataTable(tableId)) {
            $(tableId).DataTable(dtOptions.GetReportOptions(ajaxUrl, dtOptions.StudentColumns()));
        }

        showTab(this);
    });

    $('.nav a[href="#TeachersWhoHaveLeastQuantityOfStudents"]').click(function (e) {
        var tableId = '#TeachersWhoHaveLeastQuantityOfStudentsTable';
        var ajaxUrl = controllerName + "/TeachersWhoHaveLeastQuantityOfStudents";
        if (dtOptions.CanCreateDataTable(tableId)) {
            $(tableId).DataTable(dtOptions.GetReportOptions(ajaxUrl, dtOptions.TeacherColumns()));
        }

        showTab(this);
    });

    $('.nav a[href="#login"]').click(function (e) {
        var $navLink = $(this);
        var modalLoginId = 'modal-login';
        $.ajax({
            url: '/Account/Login',
            success: function (result) {
                window.ShowDialog(modalLoginId, result);
            },
            error: function (err) {
                console.log(err);
            }
        });

        $('#btn-login').off().on('click', function (ev) {
            var form = $('#form-login');
            var $loading = $('#' + modalLoginId).find('.dialog-loading-div').show();
            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                data: form.serialize(),
                success: function (result) {
                    if (result.IsSuccess) {
                        window.CloseDialog(modalLoginId);
                        $('#username-info').text(result.Message);
                        $navLink.css('display', 'none');
                        $('#link-logout').css('display', '');
                    }
                    window.ShowMessage(result.Message);
                },
                complete: function () {
                    $loading.hide();
                }
            });
        });
    });

    function showTab(tabLink) {
        var href = $(tabLink).attr('href');
        location.hash = href;
        $('.tab-pane').removeClass('active');
        $(href).addClass('active');
    }
});

